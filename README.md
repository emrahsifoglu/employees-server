## Resources

- http://brianflove.com/2016/11/08/typescript-2-express-node/
- http://mherman.org/blog/2016/11/05/developing-a-restful-api-with-node-and-typescript/#.WptDO3VubQ0
- https://github.com/RisingStack/node-typescript-starter
- https://github.com/tobymurray/express-typescript-babel
- https://github.com/jsecademy/webpack-express-typescript
- https://github.com/thomasgazzoni/graphql-server-typescript
- https://novemberde.github.io/node/2017/10/22/Express-Typescript.html
- https://www.apollographql.com/docs/apollo-server/example.html
- https://github.com/apollographql/apollo-server#comparison-with-express-graphql
- https://hackernoon.com/creating-a-structured-hot-reloadable-graphql-api-with-express-js-de62c859643
- https://scotch.io/@codediger/build-a-simple-graphql-api-server-with-express-and-nodejs
- https://stackoverflow.com/questions/44127781/graphql-and-mongoose-overwrite-property-to-null-with-not-passed-argument-in-muta
- https://github.com/glenjamin/webpack-hot-middleware/issues/89
- https://github.com/vitaliy-bobrov/angular-hot-loader/issues/5
- https://github.com/mhaagens/express_graphql_hmr_article_boilerplate
