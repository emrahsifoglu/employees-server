import {
    GraphQLObjectType,
    GraphQLList,
    GraphQLNonNull,
    GraphQLString,
    GraphQLInt,
    GraphQLID
} from 'graphql';

import { getEmployees, createEmployee, updateEmployee, deleteEmployee } from '../db/employees';

const employeeType = new GraphQLObjectType({
    name: 'Employee',
    description: 'employee',
    fields: () => ({
        _id:{
            type: new GraphQLNonNull(GraphQLID)
        },
        name: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'employee\'s first name',
        },
        surname: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'employee\'s last name',
        },
        dateOfBirth: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'employee\'s birth day',
        },
        jobTitle: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'employee\'s occupation',
        }
    }),
});

const query = {
    employees: {
        type: new GraphQLList(employeeType),
        args: {
            limit: {
                description: 'limit items in the results',
                type: GraphQLInt
            }
        },
        resolve: (root, { limit }) => getEmployees(limit)
    },
};

const mutation = {
    addEmployee: {
        type: employeeType,
        args: {
            name: {
                type: new GraphQLNonNull(GraphQLString),
                description: 'employee\'s first name',
            },
            surname: {
                type: new GraphQLNonNull(GraphQLString),
                description: 'employee\'s last name',
            },
            dateOfBirth: {
                type: new GraphQLNonNull(GraphQLString),
                description: 'employee\'s birth day',
            },
            jobTitle: {
                type: new GraphQLNonNull(GraphQLString),
                description: 'employee\'s occupation',
            },
        },
        resolve: (obj, input) => createEmployee(input)
    },
    removeEmployee: {
        type: employeeType,
        args: {
            _id: {
                type: new GraphQLNonNull(GraphQLID)
            },
        },
        resolve: (obj, input) => deleteEmployee(input)
    },
    updateEmployee: {
        type: employeeType,
        args: {
            _id: {
                type: new GraphQLNonNull(GraphQLID)
            },
            name: {
                type: GraphQLString,
                description: 'employee\'s first name',
            },
            surname: {
                type: GraphQLString,
                description: 'employee\'s last name',
            },
            dateOfBirth: {
                type: GraphQLString,
                description: 'employee\'s birth day',
            },
            jobTitle: {
                type: GraphQLString,
                description: 'employee\'s occupation',
            },
        },
        resolve: (obj, input) => updateEmployee(input)
    },
};

const subscription = {

};

export const EmployeeSchema = {
    query,
    mutation,
    subscription,
    types: [employeeType]
};
