import { GraphQLSchema, GraphQLObjectType } from 'graphql';
import { EmployeeSchema } from './employee';

export const graphqlSchema = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        fields: () => Object.assign(
            EmployeeSchema.query
        )
    }),
    mutation: new GraphQLObjectType({
        name: 'Mutation',
        fields: () => Object.assign(
            EmployeeSchema.mutation
        )
    }),
    types: [
        ...EmployeeSchema.types
    ]
});
