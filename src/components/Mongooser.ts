import * as mongoose from 'mongoose';

const MONGODB_DATABASE_NAME = 'employees';
const MONGODB_CONNECTION_URI = `mongodb://localhost:27017/${MONGODB_DATABASE_NAME}`;

class Mongooser {

    constructor() {
        (mongoose as any).Promise = global.Promise;
    }

    public connect(): void {
        mongoose.connect(MONGODB_CONNECTION_URI, { useMongoClient: true });
        mongoose.connection.on('error', function (err: any) {
            console.log('Error: Could not connect to MongoDB. Did you forget to run `mongod`?');
        }).on('open', function () {
            console.log('Connection established with MongoDB')
        });
    }
}

export default Mongooser;
