import * as express from 'express';
import * as morgan from 'morgan';
import * as cors from 'cors';
import * as errorHandler from 'errorhandler';
import * as bodyParser from 'body-parser';
import { graphQLRoutes } from '../routes/graphQL';
import Mongooser from './Mongooser';

class App {
    public app: express.Application;
    private mongooser: Mongooser;

    constructor() {
        this.app = express();
        this.mongooser = new Mongooser();
        this.middleware();
        this.routes();
    }

    private middleware(): void {
        this.mongooser.connect();

        this.app.use(cors());

        // Express morgan logs
        this.app.use(morgan('combined'));

        // Parse application/x-www-form-urlencoded
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));

        // Parse application/json
        this.app.use(bodyParser.json())
    }

    private routes(): void {
        this.app.use('/', graphQLRoutes);
        this.app.use(errorHandler());
    }

}

export default App;
