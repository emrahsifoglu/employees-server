import { Schema, model, Document } from 'mongoose';
import { BaseTime, preSaveAddBaseTime } from './base';

export interface Employee {
    name: string;
    surname: string;
    dateOfBirth: string;
    jobTitle: string;
}

export interface EmployeeModel extends Employee, BaseTime, Document {

}

const modelSchema = new Schema({
    name: { type: String, required: true },
    surname: { type: String, required: true },
    dateOfBirth: { type: String, required: true },
    jobTitle: { type: String, required: true },
});

modelSchema.pre('save', preSaveAddBaseTime);

export const EmployeeModel = model<EmployeeModel>('Employee', modelSchema);

export function getEmployees(limit = 100) {
    return EmployeeModel.find().limit(limit);
}

export function createEmployee(input: Employee) {
    return EmployeeModel.create(input);
}

export function updateEmployee(input) {
    return EmployeeModel.findByIdAndUpdate({ _id: input._id }, input);
}

export function deleteEmployee(id) {
    return EmployeeModel.findByIdAndRemove(id);
}
