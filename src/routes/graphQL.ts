import { Router, Request, Response, NextFunction } from 'express';
import * as graphqlHTTP from 'express-graphql';
import * as cors from 'cors';
import { printSchema } from 'graphql/utilities/schemaPrinter';
import { graphqlSchema } from '../schema';
import { graphiqlExpress } from 'graphql-server-express';

const router: Router = Router();

// This is en example route just to avoid error
router.get('/', (req, res) => {
    res.status(200).send('Server endpoint');
});

router.get(
    '/graphql',
    graphiqlExpress({
        endpointURL: '/graphql'
    })
);

router.post('/graphql',
    cors(),
    graphqlHTTP(request => {
        const startTime = Date.now();
        return {
            schema: graphqlSchema,
            graphiql: true,
            extensions({ document, variables, operationName, result }) {
                return { runTime: Date.now() - startTime };
            }
        };
    })
);

router.use('/schema',
    (req: Request, res: Response, next: NextFunction) => {
        res.set('Content-Type', 'text/plain');
        res.send(printSchema(graphqlSchema));
    }
);

export const graphQLRoutes: Router = router;
